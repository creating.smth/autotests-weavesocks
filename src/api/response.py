import allure
import logging


class AssertableResponse:

    def __init__(self, response):
        self._response = response
        logging.info(f"Request {response.request.body}")
        logging.info(f"Response {response.json()}")

    @allure.step("then api response should have {1}")
    def should_have(self, condition):
        condition.match(self._response)
        return self

    @allure.step
    def get_field(self, name):
        return self._response.json()[name]
