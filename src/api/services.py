import os
import requests
import json
import allure

from src.api.response import AssertableResponse


class ApiService:

    def __init__(self):
        self.base_url = os.environ["BASE_URL"]
        self.headers = {"content-type": "application/json"}


class UserApiService(ApiService):

    def __init__(self):
        super().__init__()

    @allure.step
    def create_address(self, user_address):
        return AssertableResponse(requests.post(url=self.base_url + "/addresses",
                                                data=json.dumps(user_address),
                                                headers=self.headers))

    @allure.step
    def create_user(self, user):
        return AssertableResponse(requests.post(url=self.base_url + "/register",
                                                data=json.dumps(user),
                                                headers=self.headers))

    @allure.step
    def get_user_address_by_id(self, user_id):
        return AssertableResponse(requests.get(url=self.base_url + f"/customers/{user_id}/addresses"))

    @allure.step
    def get_user_by_id(self, user_id):
        return AssertableResponse(requests.get(url=self.base_url + f"/customers/{user_id}"))

    @allure.step
    def return_all_customers(self):
        response = requests.get(url=self.base_url + "/customers").json()
        all_customers = response["_embedded"]["customer"]
        return all_customers

    @allure.step
    def return_all_customers_quantity(self):
        response = requests.get(url=self.base_url + "/customers").json()
        customers_quantity = len(response["_embedded"]["customer"])
        return f"\nTotal customers: {customers_quantity}"
