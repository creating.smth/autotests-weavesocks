from jsonpath_rw import parse
from hamcrest import assert_that, equal_to


class Condition:

    def __init__(self):
        pass

    def match(self, response):
        return


class StatusCodeCondition(Condition):

    def __init__(self, expected_code):
        super().__init__()
        self._expected_code = expected_code

    def __repr__(self):
        return f"status code is {self._expected_code}"

    def match(self, response):
        actual = response.status_code
        assert actual == self._expected_code  # OR assert_that(actual, equal_to(self._expected_code))


status_code = StatusCodeCondition


class BodyFieldCondition(Condition):

    def __init__(self, json_path, matcher):
        super().__init__()
        self._json_path = json_path
        self._matcher = matcher

    def __repr__(self):
        return f"body field [{self._json_path}] {self._matcher}"

    def match(self, response):
        json = response.json()
        value = parse(self._json_path).find(json)
        assert_that(value, self._matcher)


body = BodyFieldCondition