# remove existing "allure_result" folder
rm -rf allure_tests

# run tests with allure
pytest tests/api -sv --alluredir=allure_results

# generate report
~/.allure/allure-2.13.2/bin/allure serve allure_results
