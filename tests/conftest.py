import pytest
from selenium import webdriver
from faker import Faker


@pytest.fixture()
def faker():
    return Faker()


@pytest.fixture(scope="session", autouse=True)
def remote_driver():

    capabilities = {
        "browserName": "chrome",
        "version": "79.0",
        "enableVNC": True,
        "enableVideo": False
    }

    driver = webdriver.Remote(
        command_executor="http://localhost:4444/wd/hub",
        desired_capabilities=capabilities)
