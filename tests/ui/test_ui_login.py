import time
from selene.api import *
from selenium import webdriver
from src.api.services import UserApiService

config.base_url = "http://127.0.0.1"
config.browser_name = "chrome"


def test_can_login(faker):

    # given
    user = {"username": faker.name(), "password": faker.password(), "email": faker.email()}

    # when
    UserApiService().create_user(user)

    # driver = webdriver.Chrome()
    # driver.get("http://127.0.0.1")
    # driver.find_element_by_css_selector("#login > a").click()
    # time.sleep(3)
    # driver.find_element_by_css_selector("#login > a").send_keys(user["username"])
    # driver.find_element_by_css_selector("password-modal").send_keys(user["password"])
    # driver.find_element_by_css_selector("#login-modal > div > div > div.modal-body > form > p > button").click()

    browser.open_url("/")

    s("#login > a").click()  # "s" == "browser.element"
    time.sleep(3)
    s("username-modal").set_value(user["username"])
    s("password-modal").set_value(user["password"])
    s("#login-modal > div > div > div.modal-body > form > p > button").click()
