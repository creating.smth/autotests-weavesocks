from src.api.conditions import status_code
from src.api.services import UserApiService


"""
Constantly getting a response "{'error': 'Do: Invalid Id Hex', 'status_code': 500, 'status_text': 'Internal Server Error'}"
after making a request for creating a user address (request is valid though). 

Hence get "None" for the resp_user_address.

"resp_create_address" gets status code 200.

Simple internal error?
"""


def test_user_can_add_address(faker):

    user = {"username": faker.name(),
            "password": faker.password(),
            "email": faker.email()}

    resp_create_user = UserApiService().create_user(user)  # request-response 1

    user_id = resp_create_user.get_field("id")

    resp_create_user.should_have(status_code(200))

    user_address = {"street": faker.street_name(),
                    "number": faker.building_number(),
                    "country": faker.country(),
                    "city": faker.city(),
                    "postcode": faker.postcode(),
                    "userID": f"{user_id}"}

    resp_create_address = UserApiService().create_address(user_address)  # request-response 2

    resp_create_address.should_have(status_code(200))

    resp_user_address = UserApiService().get_user_address_by_id(user_id)  # request-response 3

    print(resp_user_address.json())
