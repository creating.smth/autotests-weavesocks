import pytest
import json
import requests
from hamcrest import greater_than, has_length, equal_to

from src.api.conditions import status_code, body
from src.api.services import UserApiService


def test_user_can_register_with_valid_credentials(faker):

    user = {"username": faker.name(), "password": faker.password(), "email": faker.email()}

    resp = UserApiService().create_user(user)

    resp.should_have(status_code(200))\
        .should_have(body("$.id", has_length(greater_than(0))))


def test_can_get_user_by_id(faker):
    user = {"username": faker.name(), "password": faker.password(), "email": faker.email()}

    resp = UserApiService().create_user(user)

    user_id = resp.get_field("id")
    resp = UserApiService().get_user_by_id(user_id)

    resp.should_have(status_code(200))\
        .should_have(body("password", equal_to("qwerty")))